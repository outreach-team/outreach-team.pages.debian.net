---
layout: post
title: "GSoC 2024 Final Report: Benchmarking Parallel Performance of Numerical MPI Packages"
date: 2024-11-07
categories: gsoc debian mpi
author: Emanuele Rocca
---

# GSoC 2024 Final Report: Benchmarking Parallel Performance of Numerical MPI Packages

Nikolaos Chatzikonstantinou published a report describing the work done [here](https://gsoc2024-parsons-ballarin----b19696eb446388731f769f725c7642be22.pages.debian.net/).
