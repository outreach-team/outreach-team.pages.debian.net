---
layout: post
title: "GSoC 2024: Improve Support of Rust findutils in Debian"
date: 2024-10-30
categories: gsoc debian rust
author: hanbings and Emanuele Rocca
---

# GSoC 2024: Improve Support of Rust findutils in Debian

## Description

[uutils/findutils](https://github.com/uutils/findutils): A safer and more performant implementation of the GNU suite’s xargs, find, locate, and updatedb tools in Rust.

Currently, findutils implements many common commands in Linux and provides a better user experience in addition to compatibility. However, the current pass number in the GNU test is still relatively low, and there are many features that have not yet been implemented. By implementing them and writing test code, compatibility with the original GNU suite will be improved, and using the Rust language will result in high maintainability and high performance.

The main work in this GSoC proposal is as follows:

- Investigate pre-2020 issues to determine if they have been fixed, implemented, or otherwise. (1 weeks)
- Identify directions for improving compatibility based on GNU testing of findutils. (9 weeks)
- Improve test code coverage of other codes in findutils. (2 weeks)

## Achievements

I documented my progress over the past two months [here](https://docs.google.com/document/d/1TqQpV4U7Z-GJYnZpJPGYvMjDxCnAE8mvoWfzhbooAQA/edit)

- **Implementing `find` features**:
    - `-anewer`, `-cnewer` [Issue](https://github.com/uutils/findutils/issues/370), [PR](https://github.com/uutils/findutils/pull/386)
    - `-context` [Issue](https://github.com/uutils/findutils/issues/375)
    - `-daystart` [Issue](https://github.com/uutils/findutils/issues/372), [PR](https://github.com/uutils/findutils/pull/413)
    - `-files0-from` [Issue](https://github.com/uutils/findutils/issues/378)
    - `-fls -ls -fprintf` [Issue](https://github.com/uutils/findutils/issues/382), [PR](https://github.com/uutils/findutils/pull/435)
    - `-fprint` [Issue](https://github.com/uutils/findutils/issues/381), [PR](https://github.com/uutils/findutils/pull/421)
    - `-fprint0` [Issue](https://github.com/uutils/findutils/issues/380), [PR](https://github.com/uutils/findutils/pull/443)
    - `-fstype` [Issue](https://github.com/uutils/findutils/issues/374), [PR](https://github.com/uutils/findutils/pull/408)
    - `-gid -uid` [Issue](https://github.com/uutils/findutils/issues/371), [PR](https://github.com/uutils/findutils/pull/405)
    - `-ignore_readdir_race -noignore_readdir_race` [Issue](https://github.com/uutils/findutils/issues/377), [PR](https://github.com/uutils/findutils/pull/411)
    - `-samefile` [Issue](https://github.com/uutils/findutils/issues/373), [PR](https://github.com/uutils/findutils/pull/389)
    - `-xtype` [Issue](https://github.com/uutils/findutils/issues/379), [PR](https://github.com/uutils/findutils/pull/436)
    - `-follow` [Issue](https://github.com/uutils/findutils/issues/308), [PR](https://github.com/uutils/findutils/pull/420)
    - `-H -L -P` [Issue](https://github.com/uutils/findutils/issues/412), [PR](https://github.com/uutils/findutils/pull/436)
    - `-noleaf` [Issue](https://github.com/uutils/findutils/issues/376), [PR](https://github.com/uutils/findutils/pull/414)

- **Other**:
    - [find: Fix -newerXY test code for Windows platform](https://github.com/uutils/findutils/pull/394)
    - [find: Fix convert_arg_to_comparable_value() function parsing unexpected characters](https://github.com/uutils/findutils/pull/361)
    - [Provide GNU test comparison comments for PRs in Github Actions](https://github.com/uutils/findutils/pull/400)

## TODO
To be honest, the goal of this GSoC is not completely complete. We still have a lot of integration testing (mainly reflected in test coverage) work to be completed. In the near future, I will continue to complete this work.

I would like to express my heartfelt gratitude to my mentors for their invaluable guidance and support. :)
