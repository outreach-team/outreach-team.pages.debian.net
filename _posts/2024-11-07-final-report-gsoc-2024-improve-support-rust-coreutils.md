---
layout: post
title: "GSoC 2024 Final Report: Improve support of the Rust coreutils in Debian"
date: 2024-11-07
categories: gsoc debian rust
author: Emanuele Rocca
---

# GSoC 2024 Final Report: Improve support of the Rust coreutils in Debian

Sreehari Prasad published a report describing the work done [here](https://docs.google.com/document/d/1Pj0QhihLwoviPpZLfpAi8rBXmKtKrBeBj0mUVB578lY/edit?tab=t.0).
