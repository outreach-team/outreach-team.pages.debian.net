---
layout: post
title: "GSoC 2024 Final Report for Android Tools Project in Debian"
date: 2024-11-07
categories: gsoc debian android
author: Anurag Kumar
---

# GSoC 2024 Final Report for Android Tools Project in Debian

This is my final report for my GSoC 2024 [project](https://summerofcode.withgoogle.com/programs/2024/projects/Gy3gXSJo).

## Introduction

The Debian Android Tools team focuses on integrating the complete Android SDK and toolchain into Debian, allowing developers to build Android apps using entirely free software.

## My Work

### WIP Packaging ###
I began packaging the latest Android SDK platform for Debian. However, this process encountered significant challenges, including major blockers related to compatibility and dependencies(very old and missing dependencies like Kotlin and Gradle).
**[LINK](https://gitlab.com/anuragxone/android-sdk-platform)**


### Signflinger Packaging ###
I successfully packaged Signflinger, a library used to sign Android APKs with V1, V2, V3, and V4 signatures. Signflinger functions as a decorator for the Zipflinger ZipArchive class, offering a straightforward integration for developers needing secure APK signing.
**[LINK](https://salsa.debian.org/anuragxone/signflinger-1)**

### Android Annotations Packaging ###
I updated and packaged the Android tools annotations package using the Maven source package, ensuring compatibility and improved functionality with the latest Android development tools.
**[LINK](https://gitlab.com/anuragxone/com-android-tools-annotations)**

### Miscellaneous ###

**https://salsa.debian.org/android-tools-team/android-platform-tools/-/merge_requests/9**

**https://salsa.debian.org/android-tools-team/android-sdk-meta/-/merge_requests/10**

I created documentation on how to build the cmdline-tools package from its source code, providing developers with clear guidance to simplify the setup to build the package using Bazel, can be a benefit in the future for making this a debian package.
**https://gitlab.com/anuragxone/cmdline-tools/-/blob/main/README.md?ref_type=heads**

## What I Learned

Here's an expanded version of the "What I Learned" section with detailed explanations for each point:

---

### What I Learned

1. **Debian Packaging Process**: I learned how Debian packages are structured and created, including the significance of the `debian/` directory and its files, such as `control`, `rules`, and `changelog`. Understanding these components helped me grasp how dependencies are managed, scripts are executed, and packages are built. This knowledge is essential for contributing to the Debian ecosystem and maintaining high-quality packages that integrate seamlessly with the operating system.

2. **Salsa CI and Package Testing**: I explored how Salsa CI, Debian's continuous integration platform, is used to automate the testing and building of packages. This process involves creating pipelines that validate code changes, run tests, and ensure that packages meet Debian standards. Learning Salsa CI taught me how to set up and interpret automated tests, ensuring the reliability and stability of packages before they are uploaded to the Debian archive.

3. **Debian Development Tools**: I became familiar with various Debian development tools like `devscripts`, `debhelper`, and `javahelper`. These tools streamline the packaging process by automating tasks such as building packages, handling dependencies, and managing installation paths. Understanding these tools enabled me to efficiently package software and contribute to the Debian community, enhancing my workflow and reducing manual errors.

4. **DFSG Licenses and Copyright Files**: I gained insights into the Debian Free Software Guidelines (DFSG) and how they apply to software licenses and copyright files. This knowledge is crucial for ensuring that packages comply with Debian's policies on free software. I learned to accurately document licensing information and create copyright files that reflect the legal status of the software, contributing to Debian's commitment to open-source principles.

5. **Lintian and Package Quality Checks**: I learned to use `lintian`, a tool that checks Debian packages for common errors and policy violations. This experience taught me how to identify and resolve issues in packages, improving their quality and adherence to Debian standards. Understanding `lintian` checks and reports allowed me to ensure that my packages were well-structured, free of bugs, and compliant with best practices.

6. **Autopkgtest and Writing Tests**: I discovered how to write and run tests using `autopkgtest`, a framework for testing Debian packages. Writing these tests helped me understand the importance of automated testing in ensuring package stability and functionality across different environments. This skill is vital for maintaining package quality over time and ensuring that changes do not introduce regressions or errors.

7. **Git-Buildpackage (GBP) for Packaging**: I learned to use `git-buildpackage` (GBP) to manage Debian packaging within Git repositories. GBP integrates Git with the Debian packaging workflow, allowing for better version control, collaboration, and tracking of changes. This tool made it easier to maintain and update packages, collaborate with other developers, and manage multiple branches and versions of a package effectively.

## Conclusion

I thoroughly enjoyed my GSoC experience and am incredibly grateful to my mentors and everyone who supported me throughout this journey. Their guidance was invaluable in helping me overcome challenges and navigate complex aspects of the project. Through this experience, I learned how to work with large codebases like AOSP, which includes numerous subprojects within a monorepo, each using different build systems to construct various Android SDK tools. This deepened my understanding of large-scale open-source projects and diverse build environments. I am thankful to Debian for providing me with this fantastic opportunity to contribute to open-source software. This experience has significantly broadened my skills, and I am eager to continue contributing to the open-source community in the future.
